import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

import { AuthService } from './../../../core/service/auth.service';
import { AuthProvider } from 'src/app/core/service/auth.types';
import { OverlayService } from 'src/app/core/service/overlay.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    authForm: FormGroup;
    authProviders = AuthProvider;
    configs = {
        isSignIn: true,
        action: 'Login',
        actionChange: 'Criar Conta'
    };
    private nameControl = new FormControl('', [Validators.required, Validators.minLength(3)]);

    constructor(
        private authService: AuthService,
        private fb: FormBuilder,
        private navCtrl: NavController,
        private route: ActivatedRoute,
        private overlayService: OverlayService) { }

    ngOnInit() {
        this.createForm();
    }

    private createForm(): void {
        this.authForm = this.fb.group({
            email: ['', [Validators.required, Validators.email]],
            senha: ['', [Validators.required, Validators.minLength(6)]]
        })
    }

    get email(): FormControl {
        return <FormControl>this.authForm.get('email');
    }

    get senha(): FormControl {
        return <FormControl>this.authForm.get('senha');
    }

    get nome(): FormControl {
        return <FormControl>this.authForm.get('nome');
    }

    changeAuthAction(): void {
        this.configs.isSignIn = !this.configs.isSignIn
        const { isSignIn } = this.configs;
        this.configs.action = isSignIn ? 'Login' : 'Criar Conta';
        this.configs.actionChange = isSignIn ? 'Criar Conta' : 'Já possui uma conta';
        !isSignIn ? this.authForm.addControl('nome', this.nameControl) : this.authForm.removeControl('nome');
    }

    async onSubmit(provider: AuthProvider): Promise<void> {
        const loading = await this.overlayService.loading()
        try {
            const credentials = await this.authService.autenticar({
                isSignIn: this.configs.isSignIn,
                provider,
                user: this.authForm.value,
            });
            this.navCtrl.navigateForward(this.route.snapshot.queryParamMap.get('redirect') || '/tasks');
        } catch (error) {
            console.log('Error', error);
            await this.overlayService.toats({
                message: error.message
            })
        } finally {
            loading.dismiss();
        }
    }
}