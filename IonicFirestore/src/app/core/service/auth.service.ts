import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User, AuthProvider, AuthOptions } from './auth.types';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    authState$: Observable<firebase.User>;

    constructor(private afAuth: AngularFireAuth) {
        this.authState$ = this.afAuth.authState;
    }

    get logado(): Observable<boolean> {
        return this.authState$.pipe(map(user => user !== null))
    }

    autenticar({ isSignIn, provider, user }: AuthOptions): Promise<auth.UserCredential> {
        let operation: Promise<auth.UserCredential>;

        if (provider !== AuthProvider.Email) {
            console.log("Entrou")
            operation = this.LogarComPopup(provider);
        } else {
            operation = isSignIn ? this.Logar(user) : this.Cadastrar(user);
        }

        return operation;
    }

    deslogar(): Promise<void> {
        return this.afAuth.auth.signOut();
    }

    private Logar({ email, senha }: User): Promise<auth.UserCredential> {
        return this.afAuth.auth.signInWithEmailAndPassword(email, senha);
    }

    private Cadastrar({ email, senha, nome }: User): Promise<auth.UserCredential> {
        return this.afAuth.auth.createUserWithEmailAndPassword(email, senha)
            .then(credentials => credentials.user.updateProfile({ displayName: nome, photoURL: null })
                .then(() => credentials)
            )
    };

    private LogarComPopup(provider: AuthProvider): Promise<auth.UserCredential> {
        let signInProvider = null;

        switch (provider) {
            case AuthProvider.Facebook:
                signInProvider = new auth.FacebookAuthProvider();
                break;
        }

        return this.afAuth.auth.signInWithPopup(signInProvider);
    }
}