export interface Tasks {

    id: string;
    title: string;
    done: boolean;
}