import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import { AuthService } from 'src/app/core/service/auth.service';
import { Firestore } from 'src/app/core/classes/firestore.class';
import { Tasks } from '../models/tasks.model';

@Injectable({
    providedIn: 'root'
})
export class TasksService extends Firestore<Tasks> {

    constructor(private authService: AuthService, db: AngularFirestore) {
        super(db);
        this.init();
    }

    private init(): void {
        this.authService.authState$.subscribe(user => {
            if (user) {
                this.setCollection(`/users/${user.uid}/tasks`, ref => ref.orderBy('done', 'asc').orderBy('title', 'asc'));
                return;
            }
            this.setCollection(null);
        });
    }
}