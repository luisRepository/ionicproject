import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Tasks } from '../../models/tasks.model';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss'],
})
export class TaskItemComponent {

@Input() task: Tasks;
@Output() done = new EventEmitter<Tasks>();
@Output() update = new EventEmitter<Tasks>();
@Output() delete = new EventEmitter<Tasks>();

}