import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { OverlayService } from 'src/app/core/service/overlay.service';
import { TasksService } from './../../services/tasks.service';
import { Tasks } from '../../models/tasks.model';
import { take } from 'rxjs/operators';

@Component({
    selector: 'app-tasks-list',
    templateUrl: './tasks-list.page.html',
    styleUrls: ['./tasks-list.page.scss'],
})
export class TasksListPage {

    tasks$: Observable<Tasks[]>;

    constructor(
        private navCtrl: NavController,
        private overlayService: OverlayService,
        private tasksService: TasksService
    ) { }

    async ionViewDidEnter(): Promise<void> {
        const loading = await this.overlayService.loading();
        this.tasks$ = this.tasksService.getAll();
        this.tasks$.pipe(take(1)).subscribe(tasks => loading.dismiss());
    }

    onUpdate(task): void {
        this.navCtrl.navigateForward(`/tasks/edit/${task.id}`);
    }

    async onDelete(task): Promise<void> {
        await this.overlayService.alert({
            message: `Do yout really want to delete the task "${task.title}"?`,
            buttons: [
                {
                    text: 'Yes',
                    handler: async () => {
                        await this.tasksService.delete(task);
                        await this.overlayService.toats({
                            message: `Task "${task.title}" deleted!`
                        })
                    }
                },
                'No'
            ]
        });
    }

    async onDone(task): Promise<void> {
        const taskToUpdate = { ...task, done: !task.done };
        await this.tasksService.update(taskToUpdate);
        await this.overlayService.toats({
            message: `Task "${task.title}" ${taskToUpdate.done ? 'completed' : 'updated'}!`
        });
    }
}