// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyDF4aTblHAP9s9RnV_5TH6hxfhQwj244GM",
        authDomain: "ionic-firestore-3a67e.firebaseapp.com",
        databaseURL: "https://ionic-firestore-3a67e.firebaseio.com",
        projectId: "ionic-firestore-3a67e",
        storageBucket: "",
        messagingSenderId: "10560833288",
        appId: "1:10560833288:web:96dbeb39c7e7d5306acebd",
        measurementId: "G-Q37WDKE7YS"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
